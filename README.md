# BVG E-Commerce Project

This is a mock e-commerce website project that implements the typical functionality of such a website, such as browsing products, adding products to a basket, editing a basket, and a mock checkout process, along with administrator functionality.

For the back-end, the project made use of C#, the ASP.NET framework, the MVC architecture, and Entity Framework, while HTML/CSS was used for the front-end.
