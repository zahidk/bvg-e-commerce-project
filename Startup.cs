﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BigVisionGames.Startup))]
namespace BigVisionGames
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
