﻿using BigVisionGames.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BigVisionGames.ViewModels
{
    public class OrderLineViewModel
    {
        public Product Product { get; set; }
        [Required]
        [Display(Name = "Quantity")]
        public int ChosenQuantity { get; set; }
    }
}