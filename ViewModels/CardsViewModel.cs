﻿using BigVisionGames.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BigVisionGames.ViewModels
{
    public class CardsViewModel
    {
        public List<Card> cards { get; set; }
        public Order order { get; set; }
    }
}