﻿using BigVisionGames.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BigVisionGames.ViewModels
{
    public class BasketViewModel
    {
        public List<OrderLine> orderlines { get; set; }
        [Display(Name = "Total")]
        public double orderTotal { get; set; }
        public Order Order { get; set; }
        
    }
}