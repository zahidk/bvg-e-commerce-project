﻿using BigVisionGames.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BigVisionGames.ViewModels
{
    public class CardOrderViewModel
    {
        public Card card { get; set; }
        public Order order { get; set; }
        [Required]
        [Display(Name="Expiry Month")]
        public int ChosenMonth { get; set; }
        [Required]
        [Display(Name = "Expiry Year")]
        public int ChosenYear{ get; set; }
    }
}