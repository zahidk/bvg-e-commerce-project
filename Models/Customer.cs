﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BigVisionGames.Models
{
    public class Customer : User
    {
        public bool IsLoyaltyMember { get; set; }
        public int LoyaltyPoints { get; set; }


        //nav props
        public List<Order> Orders { get; set; }

        public List<Card> Cards { get; set; }
    }
}