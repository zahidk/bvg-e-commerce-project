﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BigVisionGames.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        [Display(Name ="Order Date")]
        public DateTime OrderDate { get; set; }
        [Display(Name = "Order Total")]
        public double OrderTotal { get; set; }
        [Display(Name = "Order Status")]
        public OrderStatus OrderStatus { get; set; }        

        //nav props
        [ForeignKey("Customer")]
        public string CustomerId { get; set; }
        public Customer Customer { get; set; }      
        public List<OrderLine> OrderLines { get; set; }
    }

    public enum OrderStatus{
        Incomplete,
        Complete
    }
}