﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BigVisionGames.Models
{
    public class Card
    {
        public int CardId { get; set; }
        [Required]
        [Display(Name = "Cardholder Name")]
        public string CardHolderName { get; set; }
        [Required]
        [Display(Name = "Card Number")]
        public string CardNumber { get; set; }
        [Required]
        public string CVV { get; set; }
        [Required]
        [Display(Name = "Expiry Month")]
        public int ExpiryMonth { get; set; }
        [Required]
        [Display(Name = "Expiry Year")]
        public int ExpiryYear { get; set; }
        [Required]
        public bool IsExpired { get; set; }
        //public string Address { get; set; }
        //public string City { get; set; }
        //public string  Postcode { get; set; }
        //public string Country { get; set; }

        [ForeignKey("Customer")]
        public string CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}