﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BigVisionGames.Models
{
    public class OrderLine
    {
        [Key]
        public int OrderLineId { get; set; }
        public int Quantity { get; set; }
        [Display(Name ="Product Total")]
        public double LineTotal { get; set; }

        //nav props
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public Order Order { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public Product Product { get; set; }

    }
}