﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BigVisionGames.Models
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<BVGDbContext>
    {
        protected override void Seed(BVGDbContext context)
        {
            //seeding the database with roles
            if (!context.Users.Any())
            {
                RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


                if (!roleManager.RoleExists("Admin"))
                {
                    roleManager.Create(new IdentityRole("Admin"));
                }
                if (!roleManager.RoleExists("Customer"))
                {
                    roleManager.Create(new IdentityRole("Customer"));
                }
                if (!roleManager.RoleExists("CorporateCustomer"))
                {
                    roleManager.Create(new IdentityRole("CorporateCustomer"));
                }
                if (!roleManager.RoleExists("SalesAssistant"))
                {
                    roleManager.Create(new IdentityRole("SalesAssistant"));
                }
                if (!roleManager.RoleExists("InvoiceClerk"))
                {
                    roleManager.Create(new IdentityRole("InvoiceClerk"));
                }
                if (!roleManager.RoleExists("StoreManager"))
                {
                    roleManager.Create(new IdentityRole("StoreManager"));
                }
                if (!roleManager.RoleExists("AssistantManager"))
                {
                    roleManager.Create(new IdentityRole("AssistantManager"));
                }
                if (!roleManager.RoleExists("StorageManager"))
                {
                    roleManager.Create(new IdentityRole("StorageManager"));
                }
                if (!roleManager.RoleExists("StorageAssistant"))
                {
                    roleManager.Create(new IdentityRole("StorageAssistant"));
                }

                //save the changes
                context.SaveChanges();

                //****************************************
                //Creating some users and giving them roles
                //****************************************

                UserManager<User> userManager = new UserManager<User>(new UserStore<User>(context));

                //if below user does not exist
                if (userManager.FindByEmail("admin@bigvisiongames.com") == null)
                {
                    //password validator
                    userManager.PasswordValidator = new PasswordValidator()
                    {
                        RequireDigit = false,
                        RequiredLength = 1,
                        RequireLowercase = false,
                        RequireNonLetterOrDigit = false,
                        RequireUppercase = false

                    };

                    //create an ADMIN
                    var admin = new User()
                    {
                        UserName = "admin@bigvisiongames.com",
                        Email = "admin@bigvisiongames.com",
                        FirstName = "Jo",
                        LastName = "Boy",
                        AddressLine1 = "13",
                        AddressLine2 = "Good St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07432783241",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(admin, "admin123");
                    //add the user to the admin role
                    userManager.AddToRole(admin.Id, "Admin");


                    //create an CUSTOMER
                    var customer1 = new User()
                    {
                        UserName = "cust1@bigvisiongames.com",
                        Email = "cust1@bigvisiongames.com",
                        FirstName = "Seb",
                        LastName = "Laider",
                        AddressLine1 = "13",
                        AddressLine2 = "Quiet St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07432783231",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(customer1, "cust123");
                    //add the user to the CUSTOMER role
                    userManager.AddToRole(customer1.Id, "Customer");


                    //create an SALES ASSISTANT
                    var salesAssistant = new User()
                    {
                        UserName = "salesassistant@bigvisiongames.com",
                        Email = "salesassistant@bigvisiongames.com",
                        FirstName = "Daze",
                        LastName = "Beck",
                        AddressLine1 = "13",
                        AddressLine2 = "Big St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07436583231",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(salesAssistant, "sa123");
                    //add the user to the SALES ASSISTANT role
                    userManager.AddToRole(salesAssistant.Id, "SalesAssistant");

                    //create an INVOICE CLERK
                    var invoiceClerk = new User()
                    {
                        UserName = "invoiceclerk@bigvisiongames.com",
                        Email = "invoiceclerk@bigvisiongames.com",
                        FirstName = "Bert",
                        LastName = "Lome",
                        AddressLine1 = "13",
                        AddressLine2 = "Like St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07436583231",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(invoiceClerk, "ic123");
                    //add the user to the INVOICE CLERK role
                    userManager.AddToRole(invoiceClerk.Id, "InvoiceClerk");

                    //create an storeManager
                    var storeManager = new User()
                    {
                        UserName = "storemanager@bigvisiongames.com",
                        Email = "storemanager@bigvisiongames.com",
                        FirstName = "Gore",
                        LastName = "Tyke",
                        AddressLine1 = "13",
                        AddressLine2 = "Poor St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07436583231",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(storeManager, "sm123");
                    //add the user to the storeManager role
                    userManager.AddToRole(storeManager.Id, "StoreManager");

                    //create an assistantManager
                    var assistantManager = new User()
                    {
                        UserName = "assistantmanager@bigvisiongames.com",
                        Email = "assistantmanager@bigvisiongames.com",
                        FirstName = "Poart",
                        LastName = "Itly",
                        AddressLine1 = "13",
                        AddressLine2 = "Type St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07436583231",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(assistantManager, "am123");
                    //add the user to the assistantManager role
                    userManager.AddToRole(assistantManager.Id, "AssistantManager");

                    //create an storageManager
                    var storageManager = new User()
                    {
                        UserName = "storagemanager@bigvisiongames.com",
                        Email = "storagemanager@bigvisiongames.com",
                        FirstName = "Rutny",
                        LastName = "Purdy",
                        AddressLine1 = "13",
                        AddressLine2 = "Vortex St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07436583231",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(storageManager, "sm123");
                    //add the user to the storageManager role
                    userManager.AddToRole(storageManager.Id, "StorageManager");

                    //create an storageAssistant
                    var storageAssistant = new User()
                    {
                        UserName = "storageAssistant@bigvisiongames.com",
                        Email = "storageAssistant@bigvisiongames.com",
                        FirstName = "Hylex",
                        LastName = "Ulder",
                        AddressLine1 = "13",
                        AddressLine2 = "Poopy St",
                        City = "Glasgow",
                        Postcode = "G11 1GG",
                        PhoneNumber = "07436583231",
                        RegDate = DateTime.Now

                    };

                    //add a hashed password
                    userManager.Create(storageAssistant, "sa123");
                    //add the user to the storageAssistant role
                    userManager.AddToRole(storageAssistant.Id, "StorageAssistant");

                    context.SaveChanges();

                    var supplier1 = new Supplier()
                    {
                        SupplierName = "Rockstar Games",
                        Address = "555 King's Rd, Fulham, London SW6 2EB",
                        PhoneNumber = "0743241343",
                        Website = "https://www.rockstargames.com/",
                    };


                    var supplier2 = new Supplier()
                    {
                        SupplierName = "Activision",
                        Address = "7656 Executive Dr, Eden Prairie, MN 55344, United States",
                        PhoneNumber = "0740549643",
                        Website = "https://www.activision.com/",
                    };

                    var supplier3 = new Supplier()
                    {
                        SupplierName = "Electronic Arts",
                        Address = "209 Redwood Shores Parkway, Redwood City, CA 94065, USA",
                        PhoneNumber = "0740345643",
                        Website = "https://www.ea.com/",
                    };

                    var supplier4 = new Supplier()
                    {
                        SupplierName = "Microsoft",
                        Address = "Microsoft Corporation One Microsoft Way Redmond, WA 98052-6399 USA",
                        PhoneNumber = "0756345076",
                        Website = "https://www.microsoft.com/en-gb/",
                    };



                    context.Suppliers.Add(supplier1);
                    context.Suppliers.Add(supplier2);
                    context.Suppliers.Add(supplier3);
                    context.Suppliers.Add(supplier4);


                    var gta = new Game()
                    {
                        ProductName = "GTA V",
                        Genre = "Action",
                        Description = "GTA V is a first person game where the player controls 3 characters who are in the midst of crime in Los Santos",
                        ImageLink = "https://www.gtabase.com/igallery/601-700/GTA_V_Cover_PC-653-720.jpg",
                        Price = 39.99,
                        StockLevel = 10,
                        Supplier = supplier1

                    };

                    var cod = new Game()
                    {
                        ProductName = "Call of Duty: Ghosts",
                        Genre = "Action/Shooting",
                        Description = "Call of Duty: Ghosts is a first-person shooting game that follows the story of Logan Walker",
                        ImageLink = "https://images-na.ssl-images-amazon.com/images/I/91trG9oaynL._AC_SL1500_.jpg",
                        Price = 44.99,
                        StockLevel = 15,
                        Supplier = supplier2
                    };

                    var cod2 = new Game()
                    {
                        ProductName = "Call of Duty: Black Ops",
                        Genre = "Action/Shooting",
                        Description = "Call of Duty: Black Ops is a first-person shooting game that is set in cold war during the 1960s",
                        ImageLink = "https://images.g2a.com/newlayout/323x433/1x1x0/8997406dd057/590dd6ccae653a041054f1aa",
                        Price = 20.99,
                        StockLevel = 25,
                        Supplier = supplier2
                    };

                    var cod3 = new Game()
                    {
                        ProductName = "Call of Duty: Modern Warfare",
                        Genre = "Action/Shooting",
                        Description = "Call of Duty: Modern Warfare is a first-person shooting game that is set in 2011 and follows a joint mission " +
                        "of U.S. Force Reconnaissance Marinea and British SAS commandoes",
                        ImageLink = "https://upload.wikimedia.org/wikipedia/en/5/5f/Call_of_Duty_4_Modern_Warfare.jpg",
                        Price = 12.99,
                        StockLevel = 5,
                        Supplier = supplier2
                    };

                    var cod4 = new Game()
                    {
                        ProductName = "Call of Duty: Modern Warfare 2",
                        Genre = "Action/Shooting",
                        Description = "Call of Duty: Modern Warfare 2 is a first-person shooting game that follows the Task Force 141, " +
                        "a special forces unit fronted by Captain Soap MacTavish, as they hunt Vladimir Makarov",
                        ImageLink = "https://upload.wikimedia.org/wikipedia/en/d/db/Modern_Warfare_2_cover.PNG",
                        Price = 15.99,
                        StockLevel = 12,
                        Supplier = supplier2
                    };

                    var NFS1 = new Game()
                    {
                        ProductName = "Need for Speed Payback",
                        Genre = "Car Racing",
                        Description = "Need for Speed Payback is a racing game set in an open world environment of Fortune Valley; a fictional version of Las Vegas, Nevada",
                        ImageLink = "https://i.redd.it/s6q4wh7n281z.jpg",
                        Price = 34.99,
                        StockLevel = 30,
                        Supplier = supplier3
                    };

                    var mouse1 = new Electronic()
                    {
                        ProductName = "Microsoft Surface Arc Mouse",
                        Type = "Computer Peripheral",
                        Description = "Surface Arc Mouse is designed to conform to your hand and snaps flat to fit easily in your bag. " +
                        "Connects via Bluetooth. Available in a choice of rich colours to complement your style",
                        ImageLink = "https://www.windowscentral.com/sites/wpcentral.com/files/styles/large/public/field/image/2017/07/surface-arc-mouse-hero2.jpg?itok=JFPvYWbd",
                        Price = 70.99,
                        StockLevel = 15,
                        Supplier = supplier4
                    };

                    var keyboard1 = new Electronic()
                    {
                        ProductName = "Microsoft All-in-One Media Keyboard",
                        Type = "Computer Peripheral",
                        Description = "This keyboard provides comfortable typing, a built-in trackpad and customisable media hot keys",
                        ImageLink = "https://static.techspot.com/images/products/2014/keyboards/org/2016-06-08-product-12.jpg",
                        Price = 34.99,
                        StockLevel = 55,
                        Supplier = supplier4
                    };

                    var laptop1 = new Electronic()
                    {
                        ProductName = "Microsoft Surface Book 2",
                        Type = "Computer",
                        Description = "Surface Book 2 is a robust laptop, powerful tablet and portable studio that adapts to the ways you work and create best.",
                        ImageLink = "https://images-na.ssl-images-amazon.com/images/I/81r3qcA3caL._AC_SY355_.jpg",
                        Price = 859.99,
                        StockLevel = 55,
                        Supplier = supplier4
                    };

                    var tablet1 = new Electronic()
                    {
                        ProductName = "Microsoft Surface Pro X",
                        Type = "Tablet",
                        Description = "Surface Pro X is ultra-thin and always connected, combining blazing-fast LTE with 2-in-1 versatility and edge-to-edge, ultra-slim 13” touchscreen",
                        ImageLink = "https://images.idgesg.net/images/article/2019/11/primary-100819449-large.jpg",
                        Price = 999.99,
                        StockLevel = 20,
                        Supplier = supplier4
                    };

                    var card1 = new Card()
                    {
                        CardHolderName = "Mr Seb Leider",
                        CardNumber = "7320432993246234",
                        CVV = "813",
                        ExpiryMonth = 8,
                        ExpiryYear = 2022,
                        IsExpired = false,
                        CustomerId = customer1.Id
                    };


                    context.Products.Add(gta);
                    context.Products.Add(cod);
                    context.Products.Add(cod2);
                    context.Products.Add(cod3);
                    context.Products.Add(cod4);
                    context.Products.Add(NFS1);
                    context.Products.Add(mouse1);
                    context.Products.Add(keyboard1);
                    context.Products.Add(laptop1);
                    context.Products.Add(tablet1);

                    context.Cards.Add(card1);

                    context.SaveChanges();
                }
            }
        }
    }
}