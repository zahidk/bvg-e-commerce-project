﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BigVisionGames.Models
{    
        public class BVGDbContext : IdentityDbContext<User>
        {
            public DbSet<Product> Products { get; set; }
            public DbSet<Order> Orders { get; set; }
            public DbSet<OrderLine> OrderLines { get; set; }
            public DbSet<Supplier> Suppliers{ get; set; }
            public DbSet<Payment> Payments{ get; set; }
            public DbSet<Invoice> Invoices{ get; set; }
            public DbSet<Card> Cards{ get; set; }

            public BVGDbContext()
                : base("BigVisionConn", throwIfV1Schema: false)
            {
            
                Database.SetInitializer(new DatabaseInitializer());
               
            }

            public static BVGDbContext Create()
            {
                return new BVGDbContext();
            }
        }
    
}