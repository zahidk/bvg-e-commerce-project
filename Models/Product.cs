﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BigVisionGames.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        [Display(Name = "Product")]
        public string ProductName { get; set; }
        public double Price { get; set; }
        [Display(Name = "Stock Level")]
        public int StockLevel { get; set; }
        public string Description { get; set; }
        [Display(Name ="Image")]
        public string ImageLink { get; set; }

        //nav props
        //[ForeignKey("OrderLine")]
        //public int OrderLineId { get; set; }
        //public OrderLine OrderLine { get; set; }

        [ForeignKey("Supplier")]
        public int SupplierId { get; set; }
        public Supplier Supplier { get; set; }

    }
}