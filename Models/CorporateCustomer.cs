﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BigVisionGames.Models
{
    public class CorporateCustomer : Customer
    {
        public string CompanyName { get; set; }

        //nav props
        public List<Invoice> Invoices{ get; set; }

    }
}