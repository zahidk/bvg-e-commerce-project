﻿using BigVisionGames.Models;
using BigVisionGames.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BigVisionGames.Controllers
{
    public class StaffController : Controller
    {
        // GET: Staff
        private BVGDbContext context = new BVGDbContext();
        /// <summary>
        /// Displays all products along with their supplier
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //list all products along with their supplier
            var products = context.Products.Include(p => p.Supplier);
            return View(products.ToList());
        }
        public ActionResult Suppliers()
        {
            //list all products along with their supplier
            var suppliers = context.Suppliers.ToList();
            return View(suppliers);
        }

        /// <summary>
        /// Displays detaisl of the selected product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            //find the selected product in the db
            Product product = context.Products.Find(id);
            //include the supplier
            var supplier = context.Suppliers.Find(product.SupplierId);
            //find the supplier
            product.Supplier = supplier;
            return View(product);
        }

        /// <summary>
        /// Displays all previous confirmed orders
        /// </summary>
        /// <returns></returns>
        public ActionResult AllOrders()
        {
            //get all previous confirmed orders
            var orders = context.Orders.Where(o => o.OrderStatus == OrderStatus.Complete).Include(o => o.OrderLines).Include(o => o.Customer).OrderByDescending(o=>o.OrderDate).ToList();
            return View(orders);
        }
        /// <summary>
        /// Displays the details of the selected order
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AllOrderDetails(int id)
        {
            //find the order
            Order order = context.Orders.Find(id);
            //get the orders orderlines
            order.OrderLines = context.OrderLines.Where(o => o.OrderId == order.OrderId).Include(o => o.Product).ToList();
            //display the order using a viewmodel
            var viewmodel = new BasketViewModel()
            {
                Order = order,
                orderlines = order.OrderLines,
                orderTotal = order.OrderTotal
            };
            return View(viewmodel);
        }

    }
}
