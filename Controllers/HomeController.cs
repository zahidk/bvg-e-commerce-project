﻿using BigVisionGames.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net;
using Microsoft.AspNet.Identity;
using BigVisionGames.ViewModels;

namespace BigVisionGames.Controllers
{
    public class HomeController : Controller
    {
        private BVGDbContext context = new BVGDbContext();

        /// <summary>
        /// Displays a list of all available products
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var products = context.Products.Where(p => p.StockLevel > 0).ToList();
            return View(products);
        }

        /// <summary>
        /// Displays company about info
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Displays company contact info
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            return View();
        }

        /// <summary>
        /// Displays list of available games
        /// </summary>
        /// <returns></returns>
        public ActionResult Games()
        {
            //get the list of games
            var games = context.Products.OfType<Game>().Where(g => g.StockLevel > 0).ToList();
            return View(games);
        }

        /// <summary>
        /// Displays list of available electronics
        /// </summary>
        /// <returns></returns>
        public ActionResult Electronics()
        {
            //get the list of electroncics
            var electronics = context.Products.OfType<Electronic>().Where(e => e.StockLevel > 0).ToList();
            return View(electronics);
        }
        /// <summary>
        /// Displays detaisl of the selected product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            Product product = context.Products.Find(id);
            return View(product);
        }
        /// <summary>
        /// Displays account info of the logged in customer
        /// </summary>
        /// <returns></returns>
        public ActionResult AccountDetails()
        {
            string id = User.Identity.GetUserId();
            User user = context.Users.Find(id);
            return View(user);
        }
        /// <summary>
        /// Displays a list of the loggedInCustomer's confirmed orders
        /// </summary>
        /// <returns></returns>
        public ActionResult MyOrders()
        {
            //get the logged in users id
            string id = User.Identity.GetUserId();
            //get the loggedincustomers confirmed orders
            var orders = context.Orders.Where(o => o.CustomerId == id && o.OrderStatus == OrderStatus.Complete).Include(o => o.OrderLines).OrderByDescending(o => o.OrderDate).ToList();
            //send them to the view
            return View(orders);
        }
        /// <summary>
        /// Displays the details of the selected orders
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult OrderDetails(int id)
        {
            //find the order
            Order order = context.Orders.Find(id);
            //find its orderlines by orderid
            order.OrderLines = context.OrderLines.Where(o => o.OrderId == order.OrderId).Include(o => o.Product).ToList();
            //display them with a viewmodel
            var viewmodel = new BasketViewModel()
            {
                Order = order,
                orderlines = order.OrderLines,
                orderTotal = order.OrderTotal
            };
            return View(viewmodel);
        }

        /// <summary>
        /// List of the customers cards
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCards()
        {
            if (User.Identity.IsAuthenticated)
            {
                string userid = User.Identity.GetUserId();
                //list of the loggedIncustomer's cards
                var Custcards = context.Cards.Where(c => c.CustomerId == userid).ToList(); 
                //return the list of cards
                return View(Custcards);
            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Deletes the selected card
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public ActionResult DeleteCard(int? cardId)
        {
            if (cardId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //find the selected card in the database
            var card = context.Cards.Find(cardId);

            if (card == null)
            {
                return HttpNotFound();
            }

            //delete the card
            context.Cards.Remove(card);
            context.SaveChanges();
            return RedirectToAction("MyCards", "Home");
        }

    }//ends controller
}//ends namespace