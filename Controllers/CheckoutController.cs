﻿using BigVisionGames.Models;
using BigVisionGames.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net;
using Rotativa;
using System.Net.Mail;

namespace BigVisionGames.Controllers
{
    public class CheckoutController : Controller
    {
        BVGDbContext context = new BVGDbContext();

        /// <summary>
        /// Displays a list of the loggedInCustomer's cards
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns>ViewModel</returns>
        public ActionResult CardsList(int orderid)
        {
            if (User.Identity.IsAuthenticated)
            {
                string userid = User.Identity.GetUserId();
                Order Custorder = context.Orders.Find(orderid);
                //list of the loggedIncustomer's cards
                var Custcards = context.Cards.Where(c => c.CustomerId == userid).ToList();
                //if customer does not have cards, prompt them to add a new card
                if (!Custcards.Any())
                {
                    return RedirectToAction("AddNewCard", "Checkout", new { id = Custorder.OrderId });
                }
                //else display the list of cards
                else
                {
                    var viewModel = new CardsViewModel
                    {
                        cards = Custcards,
                        order = Custorder
                    };
                    return View(viewModel);
                }
            }

            return RedirectToAction("Login", "Account");

        }
        /// <summary>
        /// Deletes the selected card
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="orderId"></param>
        /// <returns>CardsList View</returns>
        public ActionResult DeleteCard(int? cardId, int? orderId)
        {
            if (cardId == null || orderId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //find the selected card in the database
            var card = context.Cards.Find(cardId);
            var order = context.Orders.Find(orderId);

            if (card == null)
            {
                return HttpNotFound();
            }

            //delete the card
            context.Cards.Remove(card);
            context.SaveChanges();
            return RedirectToAction("CardsList", new { orderid = order.OrderId });
        }

        /// <summary>
        /// Processes the checkout, creates a new payment with the selected card and current order
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ActionResult CheckoutConfirmed(int? cardId, int? orderId)
        {
            //check if the customer is logged in
            if (User.Identity.IsAuthenticated)
            {
                if (cardId == null || orderId == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //find the card in the db
                Card card = context.Cards.Find(cardId);
                //find the order in the db
                Order order = context.Orders.Find(orderId);
                string userid = User.Identity.GetUserId();
                var Custcards = context.Cards.Where(c => c.CustomerId == userid).ToList();
                if (isExpired(card) == true)
                {
                    //display error if card is expired
                    ModelState.AddModelError(string.Empty, "Card is expired");
                    var viewModel = new CardsViewModel
                    {
                        cards = Custcards,
                        order = order
                    };
                    return View(viewModel);
                }
                //get the list of orderlines of the order
                var orderlines = context.OrderLines.Where(o => o.OrderId == order.OrderId).ToList();
                foreach (var orderLine in orderlines)
                {
                    var product = context.Products.Find(orderLine.ProductId);
                    //subtract the selected quantity from the products stock level
                    product.StockLevel -= orderLine.Quantity;
                }
                //create a new payment
                Payment payment = new Payment()
                {
                    Card = card,
                    Order = order,
                    PaymentAmount = order.OrderTotal,
                    PaymentDate = DateTime.Now,
                    PaymentStatus = "Confirmed"
                };
                //update the orderstatus
                order.OrderStatus = OrderStatus.Complete;
                //update the date of order placement
                order.OrderDate = DateTime.Now;
                //add the payment to the db
                context.Payments.Add(payment);
                context.SaveChanges();
                SendEmail();

                return RedirectToAction("OrderConfirmation", "CheckOut", new { id = orderId });
            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Allows customer to add a new card to their cards' list
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ViewModel</returns>
        public ActionResult AddNewCard(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                //find the order in the db
                var order = context.Orders.Find(id);
                //return a viewmodel
                var viewmodel = new CardOrderViewModel()
                {
                    card = new Card(),
                    order = order
                };
                return View(viewmodel);
            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Adds card to db and redirects to the list of cards
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ViewModel</returns>
        [HttpPost, ActionName("AddNewCard")]
        [ValidateAntiForgeryToken]
        public ActionResult NewCardAdded(CardOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                //get the id of the loggedinuser
                string id = User.Identity.GetUserId();
                //create a new card with the entered properties
                Card newCard = new Card()
                {
                    CardHolderName = model.card.CardHolderName,
                    CardNumber = model.card.CardNumber.Replace(" ", string.Empty),
                    CVV = model.card.CVV,
                    CustomerId = id,
                    ExpiryMonth = model.ChosenMonth,
                    ExpiryYear = model.ChosenYear
                };

                if (isExpired(newCard) == true)
                {
                    //display error if card is expired
                    ModelState.AddModelError(string.Empty, "The card you have entered is expired");
                    var viewmodel = new CardOrderViewModel()
                    {
                        card = new Card(),
                        order = model.order
                    };
                    return View(viewmodel);
                }
                bool result = newCard.CardNumber.Any(x => !char.IsLetter(x));
                if (result == false || newCard.CardNumber.Length != 16)
                {
                    //display error if entry contains numbers
                    ModelState.AddModelError(string.Empty, "Invalid Card Number");
                    var viewmodel = new CardOrderViewModel()
                    {
                        card = new Card(),
                        order = model.order
                    };
                    return View(viewmodel);
                }                
                //add the card to the db
                context.Cards.Add(newCard);
                //save changes
                context.SaveChanges();
                //redirect to the cards list
                return RedirectToAction("CardsList", new { orderid = model.order.OrderId });
            }

            return View();
        }

        /// <summary>
        /// Displays confirmation of the order's placement
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult OrderConfirmation(int id)
        {
            //find the order in the db
            Order order = context.Orders.Find(id);
            //get a list of the order's orderlines
            var OrderOrderLines = context.OrderLines.Include(o => o.Product).Where(o => o.OrderId == order.OrderId);
            //display the order
            var viewModel = new BasketViewModel
            {
                orderlines = OrderOrderLines.ToList(),
                orderTotal = order.OrderTotal,
                Order = order
            };

            return View(viewModel);
        }

        /// <summary>
        /// checks if the card being used is valid
        /// </summary>
        /// <param name="card"></param>
        /// <returns>bool isExpired</returns>
        public bool isExpired(Card card)
        {
            //check if cards year is later than now, if yes card is valid
            if (card.ExpiryYear > DateTime.Now.Year)
            {
                card.IsExpired = false;
            }
            //check if card year is earlier than now, if yes card is expired
            else if (card.ExpiryYear < DateTime.Now.Year)
            {
                card.IsExpired = true;
            }
            //if we get to here cards year is the same as now, so check the month
            //if it is earlier or the same as now, card is expired
            else
            {
                if (card.ExpiryMonth <= DateTime.Now.Month)
                {
                    card.IsExpired = true;
                }
            }
            //return the card isexpired bool
            return card.IsExpired;
        }

        /// <summary>
        /// Exports the order confirmation page to a PDF
        /// </summary>
        /// <param name="id">order id</param>
        /// <returns>PDF</returns>
        public ActionResult OrderToPDF(int id)
        {
            var order = context.Orders.Find(id);

            Dictionary<string, string> cookieCollection = new Dictionary<string, string>();
            foreach (var key in Request.Cookies.AllKeys)
            {
                cookieCollection.Add(key, Request.Cookies.Get(key).Value);
            }
            return new ActionAsPdf("OrderConfirmation", new { id = order.OrderId })
            {
                FileName = "OrderConfirmation.pdf",
                Cookies = cookieCollection
            };
        }

        /// <summary>
        /// Sends an order confirmation to the user who placed the orders
        /// </summary>
        public void SendEmail()
        {
            string id = User.Identity.GetUserId();
            var user = context.Users.Find(id);
            MailMessage mail = new MailMessage();

            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("businessbvg@gmail.com");
            mail.To.Add(user.Email);
            mail.Subject = "Order Confirmation";
            mail.Body = "Hi " + user.FirstName +  ",\n\n Your Big Vision Games order has been confirmed, thank you for shopping with us! \n Big Vision Games" ;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("businessbvg@gmail.com", "bvg11zak");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

        }
    }
}
