﻿using BigVisionGames.Models;
using BigVisionGames.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net;

namespace BigVisionGames.Controllers
{
    public class BasketController : Controller
    {
        BVGDbContext context = new BVGDbContext();
        /// <summary>
        /// displays a textbox for selecting quantity for the selected product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddToBasket(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                //find the selected porduct on the basket
                Product product = context.Products.Find(id);
                var viewModel = new OrderLineViewModel
                {
                    Product = product,
                };
                return View(viewModel);
            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// adds the product to an orderline and that ordeline to the current order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ActionName("AddToBasket")]
        [ValidateAntiForgeryToken]
        public ActionResult AddToBasketConfirmed(OrderLineViewModel model)
        {
            if (ModelState.IsValid)
            {
                //bool to check if orderline is new
                bool isNew = false;
                //new product is the selected product
                Product product = context.Products.Find(model.Product.ProductId);
                //create a new order
                Order order = FindLatestOrder();
                //check if the product is already in the basket
                OrderLine actualOrderLine = FindProductInBasket(order.OrderId, product.ProductId);
                if (actualOrderLine == null)
                {
                    //create a new orderline
                    actualOrderLine = new OrderLine
                    {
                        Order = order,
                        Product = product,
                    };
                    //orderline is new
                    isNew = true;
                }

                //set the orderlines quantity to the selected quantity
                actualOrderLine.Quantity += model.ChosenQuantity;
                //check if valid entry
                if (model.ChosenQuantity < 1)
                {
                    //display error if quantity is more than stock level
                    ModelState.AddModelError(string.Empty, "Invalid Entry");
                    var viewModel = new OrderLineViewModel
                    {
                        Product = product,
                    };
                    return View(viewModel);
                }
                //Check stock level vs quantity added
                if (actualOrderLine.Quantity > actualOrderLine.Product.StockLevel)
                {
                    //display error if quantity is more than stock level
                    ModelState.AddModelError(string.Empty, "Not enough inventory in stock");
                    var viewModel = new OrderLineViewModel
                    {
                        Product = product,
                    };
                    return View(viewModel);
                }
                //add the orderline to the db if it is new
                if (isNew == true)
                {
                    context.OrderLines.Add(actualOrderLine);
                    context.SaveChanges();
                }
                //order status is incomplete
                order.OrderStatus = OrderStatus.Incomplete;
                context.SaveChanges();
                //the line total is the quantity of the selected product multiplied by its price
                actualOrderLine.LineTotal = actualOrderLine.Quantity * actualOrderLine.Product.Price;
                //calcualte the order total
                order.OrderTotal = CalculateOrderTotal(order);
                context.SaveChanges();

                return RedirectToAction("AddConfirm", "Basket", new { id = model.Product.ProductId });

            }
            return View();
        }
        /// <summary>
        /// displays the order and its orderlines
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewBasket()
        {
            if (User.Identity.IsAuthenticated)
            {
                //create a new order
                Order order = new Order();
                //set it the latest order
                order = FindLatestOrder();
                //get the orderlines of the latest order
                var OrderOrderLines = context.OrderLines.Include(o => o.Product).Where(o => o.OrderId == order.OrderId);
                //return the basket viewmodel
                var viewModel = new BasketViewModel
                {
                    orderlines = OrderOrderLines.ToList(),
                    orderTotal = order.OrderTotal,
                    Order = order
                };

                return View(viewModel);
            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// confirmation of product being added to the basket
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddConfirm(int id)
        {
            //find the product in db by id
            Product product = context.Products.Find(id);
            return View(product);
        }

        /// <summary>
        /// finds the latest order 
        /// </summary>
        /// <returns></returns>
        public Order FindLatestOrder()
        {
            //create a new order
            Order custOrder = new Order();
            //get the logged in customers id
            string id = User.Identity.GetUserId();
            //get the loggedincustomers orders by their id
            var orders = context.Orders.Where(o => o.CustomerId == id).ToList();
            //if there are no incomplete orders, create a new order
            if (!orders.Exists(o => o.OrderStatus == OrderStatus.Incomplete))
            {
                custOrder.CustomerId = id;
                custOrder.OrderDate = DateTime.Now;
                custOrder.OrderTotal = CalculateOrderTotal(custOrder);
                context.Orders.Add(custOrder);
                context.SaveChanges();
            }
            //else find the latest incomplete order by date
            else
            {
                foreach (var item in orders)
                {
                    //check if order is the latest by date and is incomplete
                    if (item.OrderDate > custOrder.OrderDate && item.OrderStatus == OrderStatus.Incomplete)
                    {
                        custOrder = item;
                    }
                }
            }
            //return the order
            return custOrder;
        }
        /// <summary>
        /// checks if the product is already in the basket
        /// </summary>
        /// <param name="orderid"></param>
        /// <param name="productid"></param>
        /// <returns>null or an orderline</returns>
        public OrderLine FindProductInBasket(int orderid, int productid)
        {
            //find the orderline where the orderid is of the current order and the productid is of the selected product 
            var orderLine = context.OrderLines.SingleOrDefault(c => c.OrderId == orderid && c.ProductId == productid);
            //return the orderline
            return orderLine;
        }

        /// <summary>
        /// calculates the ordertotal based on the linetotal of each orderline
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public double CalculateOrderTotal(Order order)
        {
            double ordertotal = 0.00;
            //find the current order's orderlines
            var orderlines = context.OrderLines.Where(o => o.OrderId == order.OrderId);
            foreach (var item in orderlines)
            {
                //increment the ordertotal with each linetotal of its orderlines
                ordertotal += item.LineTotal;
            }
            order.OrderTotal = ordertotal;
            //return the ordertotal
            return order.OrderTotal;
        }

        /// <summary>
        /// removes selected product from the basket
        /// </summary>
        /// <param name="prodId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ActionResult DeleteFromBasket(int? prodId, int? orderId)
        {
            if (prodId == null || orderId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //find the correct orderline based on the productid and orderid
            var orderLine = context.OrderLines.Include(o => o.Product).SingleOrDefault(o => o.OrderId == orderId && o.ProductId == prodId);

            if (orderLine == null)
            {
                return HttpNotFound();
            }

            return View(orderLine);
        }

        /// <summary>
        /// deletes product from basket
        /// </summary>
        /// <param name="prodId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPost, ActionName("DeleteFromBasket")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int prodId, int orderId)
        {
            //find the orderline
            OrderLine orderline = context.OrderLines.Where(o => o.ProductId == prodId && o.OrderId == orderId).ToList().First();
            //find the order
            Order order = context.Orders.Find(orderId);
            //remove the orderline from the basket
            context.OrderLines.Remove(orderline);
            context.SaveChanges();
            //calculate the new ordertotal
            order.OrderTotal = CalculateOrderTotal(order);
            context.SaveChanges();
            //redirect to updated basket
            return RedirectToAction("ViewBasket", "Basket");
        }

        /// <summary>
        /// edit the quantity of a product that is already in the basket
        /// </summary>
        /// <param name="id"></param>
        /// <returns>view with orderline</returns>
        public ActionResult EditOrderline(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //find the orderline by the id
            var orderline = context.OrderLines.Where(o=>o.OrderLineId == id).Include(o=>o.Product).Include(o=>o.Order).FirstOrDefault();

            if (orderline == null)
            {
                return HttpNotFound();
            }
            //return the view with the orderline
            return View(orderline);
        }

        [HttpPost, ActionName("EditOrderLine")]
        [ValidateAntiForgeryToken]
        public ActionResult EditConfirmed(OrderLine orderLine)
        {
            if (ModelState.IsValid)
            {
                //find the prodcutid in the orderline
                var product = context.Products.Find(orderLine.ProductId);
                //find the orderid of the orderline
                var order = context.Orders.Find(orderLine.OrderId);
                //find the order and product objects
                orderLine.Product = product;
                orderLine.Order = order;

                if (orderLine.Quantity < 1)
                {
                    //display error if quantity is more than stock level
                    ModelState.AddModelError(string.Empty, "Invalid Entry");
                    return View(orderLine);
                }
                //Check stock level vs quantity added
                if (orderLine.Quantity > orderLine.Product.StockLevel)
                {
                    //display error if quantity is more than stock level
                    ModelState.AddModelError(string.Empty, "Not enough inventory in stock");                    
                    return View(orderLine);
                }
                //calculate the line total
                orderLine.LineTotal = orderLine.Product.Price * orderLine.Quantity;
                context.Entry(orderLine).State = EntityState.Modified;
                context.SaveChanges();
                //calculate the order total
                order.OrderTotal = CalculateOrderTotal(order);
                context.SaveChanges();
                return RedirectToAction("ViewBasket", "Basket");
            }

            return View();
        }

        public ActionResult ClearBasket(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //find the selected card in the database
            var order = context.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            //find the orderlines of the current order
            var orderlines = context.OrderLines.Where(o => o.OrderId == order.OrderId);

            //remove each orderline in the order one by one
            foreach (var item in orderlines)
            {
                context.OrderLines.Remove(item);
            }
            //save changes
            context.SaveChanges();
            return RedirectToAction("ViewBasket", "Basket");
        }


    }
}